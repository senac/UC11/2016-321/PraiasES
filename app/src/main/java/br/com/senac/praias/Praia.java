package br.com.senac.praias;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by sala304b on 24/08/2017.
 */

public class Praia implements Serializable {
    private int id;
    private String nome;
    private String informacao;
    private int imagem;

    public Praia() {
    }

    public Praia(int id, String nome, String informacao) {
        this.id = id;
        this.nome = nome;
        this.informacao = informacao;
    }

    public Praia(int id, String nome, String informacao, int imagem) {
        this.id = id;
        this.nome = nome;
        this.informacao = informacao;
        this.imagem = imagem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getInformacao() {
        return informacao;
    }

    public void setInformacao(String informacao) {
        this.informacao = informacao;
    }

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }

    @Override
    public String toString() {
        return this.nome;
    }


}
